#include "People.h"

std::string Person::getName()
{
  return std::string("Somebody");
}

std::string Charlie::getName()
{
  return std::string("Charlie");
}

std::string Chen::getName()
{
  return std::string("Chen");
}

std::string Devin::getName()
{
	return std::string("Devin");
}

std::string Joe::getName()
{
  return std::string("Joe");
}

std::string George::getName()
{
  return std::string("George");
}

std::string George2::getName()
{
  return std::string("George2");
}

std::string George3::getName()
{
  return std::string("George3");
}

std::string George4::getName()
{
  return std::string("George4");
}

std::string George5::getName()
{
  return std::string("George5");
}

std::string David::getName()
{
  return std::string("David");
}

std::string Cameron::getName()
{
  return std::string("Cameron");
}

std::string Manny::getName()
{
  return std::string("Manny");
}

std::string YenTing::getName()
{
  return std::string("Yen Ting");
}

std::string Tianchen::getName()
{
  return std::string("Tianchen");
}

std::string Kamil::getName()
{
  return std::string("Kamil");
}
