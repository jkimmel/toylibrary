#pragma once

#include <string>

class Person
{
public:
  virtual std::string getName();
};

class Charlie : public Person
{
  virtual std::string getName() override;
};

class Chen : public Person
{
  virtual std::string getName() override;
};

class Devin : public Person
{
	virtual std::string getName() override;
};

class Joe : public Person
{
  virtual std::string getName() override;
};

class George : public Person
{
  virtual std::string getName() override;
};

class George2 : public Person
{
  virtual std::string getName() override;
};

class George3 : public Person
{
  virtual std::string getName() override;
};

class George4 : public Person
{
  virtual std::string getName() override;
};

class George5 : public Person
{
  virtual std::string getName() override;
};

class David : public Person
{
  virtual std::string getName() override;
};

class Cameron : public Person
{
  virtual std::string getName() override;
};

class Manny : public Person
{
  virtual std::string getName() override;
};

class YenTing : public Person
{
  virtual std::string getName() override;
};

class Tianchen : public Person
{
  virtual std::string getName() override;
};

class Kamil : public Person
{
  virtual std::string getName() override;
};
